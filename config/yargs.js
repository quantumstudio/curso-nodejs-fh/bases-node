const argv = require('yargs')
  .option('b', {
    alias: 'base',
    type: 'number',
    demandOption: true,
    describe: "It's the base of the multiplication table"
  })
  .option('l', {
    alias: 'list',
    type: 'boolean',
    default: false,
    describe: "Show the multiplication table in console"
  })
  .option('t', {
    alias: 'to',
    type: 'number',
    default: 10,
    describe: "It's the multiplier of the multiplication table"
  })
  .check((argv, options) => {
    if (isNaN(argv.base)) {
      throw new Error('The base must be a number');
    }

    return true;
  })
  .argv;

module.exports = argv;
