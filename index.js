const { createTableFile } = require('./helpers/multiply');
const argv = require('./config/yargs');

require('colors');

console.clear();

const { base, list, to } = argv;

createTableFile(base, list, to)
  .then((message) => console.log(message.rainbow))
  .catch((err) => console.error(err));
