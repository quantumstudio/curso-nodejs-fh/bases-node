# Notes

My first console program in NodeJS

```
Options:
      --help     Show help                                             [boolean]
      --version  Show version number                                   [boolean]
  -b, --base     It's the base of the multiplication table   [number] [required]
  -l, --list     Show the multiplication table in console
                                                      [boolean] [default: false]
  -t, --to       It's the multiplier of the multiplication table
                                                          [number] [default: 10]
```
