const fs = require('fs');

const colors = require('colors');

const createTableFile = (base = 5, list = false, to = 10) => {
  let out = '';
  let consola = '';

  for (let index = 1; index <= to; index++) {
    consola += `${base} ${colors.green('x')} ${index} ${'='.green} ${base * index}\n`;
    out += `${base} x ${index} = ${base * index}\n`;
  }

  if (list) {
    console.log(consola);
  }

  return new Promise((resolve, reject) => {
    try {
      fs.writeFileSync(`./out/table-${base}.txt`, out);
      resolve(`table-${base}.txt created`);
    } catch (error) {
      reject(`Error to create table-${base}.txt file`);
    }
  });
};


module.exports = {
  createTableFile
};
